#! /bin/sh

# Install Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

#Install xQuarts and wine
brew cask install xquartz wine-stable

echo "USER ACTION IS REQUIRED."
echo "OPEN (double click) DCTeach.exe (open with Wine)"