
# WineComp
## Small Toolkit for users of the application 'DairyComp' on macOS
### Start by opening a terminal. Open spotlight and search for terminal, open that application
### Run the command below to download the toolkit
> cd ~ && git clone https:/gitlab.com/vulkur/winecomp.git
### Once it is finished type the line below to install prereqs. It will ask for your password.
> cd ~/winecomp && sudo ./install.sh
### Once that command is finished, you must open the downloaded .exe file (with Wine) from wherever you downloaded it. DONT CLOSE THE TERMINAL.
### Follow the standard installation process for DairyComp.
### Once it is finished, there might be some errors. That should be ok.
### Now run the following command.
> ./createDesktopIcon.sh
### Now verify everything is installed correctly by checking your desktop for a 'DairComp' app.
### Try opening it.
### MacOS has some issues with wine still, so once you are done using DairyComp, make sure to clean up the system with the following command.
> cd ~/winecomp && ./murderOphans.sh